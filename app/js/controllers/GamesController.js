module.exports					= function ($state, GameFactory, gamesResponseArray, templatesResponse)
{
	var scope					= this;

	scope.games 				= GameFactory.mapGames(gamesResponseArray);
	scope.paginationIndex 		= GameFactory.getPaginationIndex();
	scope.paginationPages 		= GameFactory.getPaginationPages();
	scope.paginationMinPage 	= GameFactory.getPaginationMinPage();
	scope.paginationMaxPage 	= GameFactory.getPaginationMaxPage();
	scope.templates 			= GameFactory.mapTemplates(templatesResponse);

	scope.defaultGame 			= {
		template: null,
		minPlayers: 1,
		maxPlayers: 1
	};

	scope.game 					= angular.copy(scope.defaultGame);

	scope.increasePaginationIndex 	= function ()
	{
		scope.setPaginationIndex(parseInt(parseInt(scope.paginationIndex) + 1));
	}

	scope.decreasePaginationIndex 	= function ()
	{
		scope.setPaginationIndex(parseInt(parseInt(scope.paginationIndex) - 1));
	}

	scope.setPaginationIndex 	= function (newPaginationIndex)
	{
		if (newPaginationIndex > 0 && newPaginationIndex <= scope.paginationMaxPage && newPaginationIndex != scope.paginationIndex)
		{
			$state.go(".", { paginationIndex: newPaginationIndex });
		}
	}

	scope.createNewGame 		= function (newGame)
	{
		var game 				= angular.copy(newGame);

		game.templateName 		= game.template.id;

	    if (game.minPlayers > game.maxPlayers) 	{ return; }

		GameFactory.createGame(game).then(function (response)
		{
			scope.games 		= GameFactory.addGameToGames(response);
			scope.reset();
		});
	};

	scope.reset 				= function()
	{
		scope.game 				= angular.copy(scope.defaultGame);
		scope.game.template 	= scope.templates[0];
	};

	scope.joinGame 				= function (gameToJoin, $event)
	{
		gameToJoin.join();
		$event.target.style.display = "none";
	};

	scope.watchGame 				= function (gameToWatch, $event)
	{
		GameFactory.setWatchingGame(gameToWatch);
		$state.go("main.game.board", { id: gameToWatch._id });
	};

	scope.startGame 				= function (gameToSart, $event)
	{
		gameToSart.start();
		$event.target.style.display = "none";
	};
};