module.exports						= function ($scope, GameFactory, TileFactory, SocketFactory, game)
{
	var scope						= this;

	scope.game 						= game;
	scope.modal						= { title: "", body: "" };

	if (scope.game && scope.game !== GameFactory.getWatchingGame())
	{
		scope.game 					= GameFactory.getGameFromResponse(scope.game);
		GameFactory.setWatchingGame(scope.game);
	}


	$scope.loadBoard 				= function ()
	{
		if (scope.game && scope.game.id && !scope.game.waitingToStart)
		{
			TileFactory.loadTiles(scope.game.id).then(function (response)
			{
				TileFactory.mapTiles(response);

				TileFactory.loadMatchedTiles(scope.game.id).then(function (matchedResponse)
				{
					scope.game.setGrid(TileFactory.mapMatchedTiles(matchedResponse));

					var matchedTiles 	= TileFactory.getMatchedTiles();

					for (var matchedTileIndex = 0; matchedTileIndex < matchedTiles.length; matchedTileIndex++)
					{
						scope.game.addMatchedPair(matchedTiles[matchedTileIndex]);
					}
				});
			});
		}
	}

	$scope.loadBoard();

	$scope.checkSelectable 			= function (tile)
	{
		return scope.game.checkSelectable(tile);
	};

    $scope.addToMatched 			= function (tilePair)
    {
    	TileFactory.setMatched(tilePair)
    	.then(function (response)
    	{
    		if (response && !response.code)
    		{
    			tilePair[0].match 		= response[0].match;
    			tilePair[1].match 		= response[1].match;
    		}
    		else
    		{
    			scope.modal.title 	= "Error from the server!";
				scope.modal.body 	= "Error code:" + response.code + "<br/>Message:" + response.message;
				scope.modal.show 	= true;
    		}
    	});

    	scope.game.addMatchedPair(tilePair);
    }

    $scope.getSelection 			= function ()
    {
    	return scope.game.getSelection();
    }

    $scope.addSelected 				= function (selectedTile)
    {
    	scope.game.addSelected(selectedTile);
    }

    $scope.resetSelection 			= function ()
    {
    	scope.game.resetSelection();
    }

    $scope.setSelected 				= function (tile, bool)
	{
		tile.selected = bool;
	}

	$scope.setMismatch 				= function ( bool)
	{
		angular.forEach(scope.game.getSelection(), function (tile)
		{
			tile.mismatched 		= bool;
		});
	}

	$scope.checkMatch 				= function (firstTile, secondTile)
	{
		if (firstTile.tile.suit == secondTile.tile.suit)
		{
	 		if (firstTile.tile.suit != "Flower" && firstTile.tile.suit != "Season")
	 		{
				return (firstTile.tile.name == secondTile.tile.name);
			}

			return true;
		}

		return false;
	}

	/* Websocket: wait until the conenction is made, then start listing to events. */
    if (scope.game && scope.game.id && SocketFactory.isConnected(scope.game.id))
    {
	    SocketFactory.on('connect', function ()
		{
			SocketFactory.on('start', function (data)
			{
				scope.game.setStarted();
				$scope.loadBoard();
				scope.modal.title 	= "Game " + scope.game.id;
				scope.modal.body 	= "The game " + scope.game.id + " has started.";
				scope.modal.show 	= true;
			});

			SocketFactory.on('end', function (data)
			{
				scope.game.setFinished();
				scope.modal.title 	= "Game " + scope.game.id;
				scope.modal.body 	= "The game " + scope.game.id + " has finished.";
				scope.modal.show 	= true;
			});

			SocketFactory.on('playerJoined', function (data)
			{
				scope.game.addPlayer(data);
				scope.game.addActivityLog(data._id + " has joined the game.");
			});

			SocketFactory.on('match', function (data)
			{
				var matchedPairs 	= scope.game.getMatchedPairs();

				var matched 		= false;

				for (var pairIndex = 0; pairIndex < matchedPairs.length; pairIndex++)
				{
					for (var tileIndex = 0; tileIndex < matchedPairs[pairIndex].length; tileIndex++)
					{
						for (var dataIndex = 0; dataIndex < data.length; dataIndex++)
						{
							if (matchedPairs[pairIndex][tileIndex]._id == data[dataIndex]._id)
							{
								matched 	= true;
								dataIndex 	= data.length;
								tileIndex  	= matchedPairs[pairIndex].length;
								pairIndex 	= matchedPairs.length - 1;
							}
						}
					}
				}

				if (!matched)
				{
					var pair 				= [];

					angular.forEach(scope.game.getGrid(), function (level)
					{
						angular.forEach(level, function (row)
						{
							angular.forEach(row, function (tile)
							{
								for (var dataIndex = 0; dataIndex < data.length; dataIndex++)
								{
									if (tile._id == data[dataIndex]._id)
									{
										tile.match 			= data[dataIndex].match;
										tile.matched 		= true;
										pair.push(tile);
									}
								}
							});
						});
					});

					angular.forEach(scope.game.getSelection(), function (selectedTile)
					{
						for (var dataIndex = 0; dataIndex < data.length; dataIndex++)
						{
							if (selectedTile._id == data[dataIndex]._id)
							{
								$scope.setSelected(selectedTile, false);
								$scope.setMismatch(false);
								scope.game.getSelection().splice(scope.game.getSelection().indexOf(selectedTile), 1);
							}
						}
					});

					scope.game.addMatchedPair(pair);
				}
			});
		});
	}
};