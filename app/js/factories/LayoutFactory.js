module.exports 				= function ()
{
	var layouts = [
      { name: 'White tiles', url: 'default' },
      { name: 'Red tiles', url: 'red' }
    ];
	var layout = 'default';

	
	return {
		getLayouts: function ()
		{
			return layouts;
		},
		setLayouts: function (newLayouts)
		{
			layouts = newLayouts;
		},
		getLayout: function ()
		{
			return layout;
		},
		setLayout: function (newLayout)
		{
			layout = newLayout;
		}
	};
};