module.exports 								= function (HttpLoader, Game, User)
{
	var games 								= [];
	var watchingGame 						= null;
	var startingPaginationPages 			= [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
	var paginationMinPage 					= startingPaginationPages[0];
	var paginationMaxPage 					= startingPaginationPages[startingPaginationPages.length - 1];
	var paginationIndex 					= paginationMinPage;
	var paginationExtendingRTLButtonIndex 	= 3;
	
	function sortGames (games)
	{
		return games.sort(dynamicSortMultiple("-state", "startedOn"));
	}

	function mapGames (response)
	{
		if (response)
		{
			games 							= [];

			angular.forEach(response, function (category, categoryKey)
			{
				angular.forEach(category, function (gameArray, gameArrayKey)
				{
					angular.forEach(gameArray.value, function (game, gameKey)
					{
						games.push(new Game(game));
					});
				});
			});
		}

		return sortGames(games);
	}

	function createGame (gameData)
	{
		return HttpLoader.post("/games", gameData);
	}

	function dynamicSort (property)
	{
		var sortOrder 						= 1;
		
		if (property[0] === "-")
		{
			sortOrder 						= -1;
			property 						= property.substr(1);
		}

		var returnFunction 					= function (a,b)
		{
			var result 						= (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;

			return result * sortOrder;
		}

		return returnFunction;
	}

	function dynamicSortMultiple ()
	{
		var props 							= arguments;

		var returnFunction 					= function (obj1, obj2)
		{
			var i 							= 0,
			result 							= 0,
			numberOfProperties 				= props.length;
			
			while (result === 0 && i < numberOfProperties)
			{
				result 						= dynamicSort(props[i])(obj1, obj2);
				i++;
			}

			return result;
		}

		return returnFunction;
	}

	function getGameById (gameId)
	{
		for (var i = 0; i < games.length; i++)
		{
			if (gameId == games[i].id)
			{
				return games[i];
			}
		}
	}

	function calculatePaginationPages ()
	{
		var calculatedPagination 			= startingPaginationPages;

		if (paginationIndex > startingPaginationPages[(startingPaginationPages.length - paginationExtendingRTLButtonIndex)])
		{
			calculatedPagination 			= [];
			var increament 					= paginationIndex - startingPaginationPages[(startingPaginationPages.length - paginationExtendingRTLButtonIndex)];

			for (var index = 0; index < startingPaginationPages.length; index++)
			{
				calculatedPagination.push(startingPaginationPages[index] + increament);
			}
		}

		paginationMinPage 					= calculatedPagination[0];
		paginationMaxPage 					= calculatedPagination[calculatedPagination.length - 1];
		
		return calculatedPagination;
	}

	function mapTemplates (response)
	{
		if (response)
		{
			var templates 					= [];

			for(var i = 0; i < response.length; i++)
			{
				templates.push(response[i]);
			}
		}

		return templates;
	}

	return {
		loadGames							: function (newPageIndex)
		{
			paginationIndex 				= (newPageIndex || paginationIndex);

			var tabbedGames 				= {};

			tabbedGames.open 				= HttpLoader.get("/games?pageIndex=" + (paginationIndex - 1) + "&state=open");
			tabbedGames.playing 			= HttpLoader.get("/games?pageIndex=" + (paginationIndex - 1) + "&state=playing");
			tabbedGames.finished 			= HttpLoader.get("/games?pageIndex=" + (paginationIndex - 1) + "&state=finished");
			tabbedGames.myopengames 		= HttpLoader.get("/games?pageIndex=" + (paginationIndex - 1) + "&player=" + User.getEmail() + "&state=open");
			tabbedGames.myplayinggames 		= HttpLoader.get("/games?pageIndex=" + (paginationIndex - 1) + "&player=" + User.getEmail() + "&state=playing");
			tabbedGames.myfinishedgames 	= HttpLoader.get("/games?pageIndex=" + (paginationIndex - 1) + "&player=" + User.getEmail() + "&state=finished");

			return tabbedGames;
		},
		getPaginationIndex 					: function ()
		{
			return paginationIndex;
		},
		getPaginationPages 					: function ()
		{
			return calculatePaginationPages();
		},
		getPaginationMinPage 				: function ()
		{
			return paginationMinPage;
		},
		getPaginationMaxPage 				: function ()
		{
			return paginationMaxPage;
		},
		mapGames 							: function (response)
		{
			return mapGames(response);
		},
		getGames 							: function ()
		{
			return games;
		},
		createGame 							: function (gameData)
		{
			return createGame(gameData);
		},
		addGameToGames 						: function (response)
		{
			var game 						= new Game(response);

			games.push(game);

			return sortGames(games);
		},
		setWatchingGame 					: function (game)
		{
			watchingGame 					= game;
		},
		getWatchingGame 					: function ()
		{
			return watchingGame;
		},
		getGameById 						: function (gameId)
		{
			return getGameById(gameId);
		},
		loadGame 							: function (gameId)
		{
			return HttpLoader.get("/games/" + gameId);
		},
		getGameFromResponse 				: function (response)
		{
			return new Game(response);
		},
		mapTemplates 						: function (templatesReponse)
		{
			return mapTemplates(templatesReponse);
		}
	};
};