module.exports 			= function (UserFactory)
{
	return {
		request: function (config)
		{
			if (UserFactory.isLoggedIn())
			{
				config.headers['x-username'] 	= UserFactory.getEmail();
				config.headers['x-token'] 		= UserFactory.getToken();
			}

			return config;
		}
	};
};