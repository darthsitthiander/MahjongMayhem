module.exports 			= function (HttpLoader)
{
	return {
		loadTemplates: function ()
		{
			return HttpLoader.get("/gameTemplates");
		}
	};
};