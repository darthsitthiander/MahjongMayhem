module.exports 				= function ()
{
	return {
		restrict 	: 'A',
		scope 		: 'GameController',
		link 		: function (scope, element, attrs)
		{
			element.bind('click', function ()
			{
				if (scope.tile.matched)
				{
					element.addClass('hidden');
				}

				// check if tile can be selected
				//var adjacentTiles = scope.getAdjacentTiles(scope.tile);
				//if(!adjacentTiles.onTop && !(adjacentTiles.top && adjacentTiles.right && adjacentTiles.bottom && adjacentTiles.left)){
				if (scope.checkSelectable(scope.tile))
				{
					// check if tile already selected
					var index 			= scope.getSelection().indexOf(scope.tile);
					if (index > -1)
					{
						scope.setMismatch(false);
						scope.setSelected(scope.tile, false);
						scope.getSelection().splice(index, 1);
					}
					else
					{
						// tile not yet selected
						if (scope.getSelection().length < 2)
						{
							// if this is the tile selected third 
							scope.setSelected(scope.tile, true);
							scope.addSelected(scope.tile);

							if (scope.getSelection().length == 2)
							{
								// if this is the tile selected second
								// tiles matched
								if (scope.checkMatch(scope.getSelection()[0], scope.tile))
								{
									element.parent().children().removeClass('selected');
									element.removeClass('selected');
									scope.addToMatched(scope.getSelection());

									angular.forEach(scope.getSelection(), function (valueS, keyS)
									{
										angular.forEach(element.parent().children(), function (valueE, keyE)
										{
											if (valueE.id == valueS.tile._id)
											{
												valueE.remove();
											}
										});

										valueS.matched 		= true;
								  	});

									scope.$apply();
							  		scope.resetSelection();
								}
								else
								{
									scope.setMismatch(true);
								}
							}
						}
					}
				}
				scope.$apply();
			});
		}
	};
};