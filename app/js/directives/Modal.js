module.exports 				= function ($parse)
{
	return {
		restrict 			: 'E',
		templateUrl 		: 'templates/modal.html',
		link: function (scope, element, attrs)
		{
			scope.title 	= "";
			scope.body 		= "";
			
			scope.showModal = function (visible, elem)
			{
				if (!elem)
				{
					elem = element;
				}

				if (visible)
				{
					$(elem).modal("show");                     
				}
				else
				{
					$(elem).modal("hide");
				}
			}

			scope.$watch(attrs.show, function (newValue)
			{
				scope.showModal(newValue, attrs.$$element);
			});

			scope.$watch(attrs.title, function (newValue)
			{
				scope.title = newValue;
			});

			scope.$watch(attrs.body, function (newValue)
			{
				scope.body = newValue;
			});

			$(element).bind("hide.bs.modal", function ()
			{
				$parse(attrs.show).assign(scope, false);

				if (!scope.$$phase && !scope.$root.$$phase)
				{
					scope.$apply();
				}
			});
		}

	};
};