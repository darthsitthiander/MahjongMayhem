/* 	==================================
				Requirements
	================================== */
require('angular/angular');
require('angular-route/angular-route');
require('angular-ui-router/build/angular-ui-router');

/* 	==================================
				Module
	================================== */
var app 								= angular.module('mahjong', ['ngRoute', 'ui.router']);

/* 	==================================
				Models
	================================== */
app.factory('User', 					[ require('./models/User') ]);

app.factory('Player', 					[ require('./models/Player') ]);

app.factory('Game', 					[ 'Player', 'HttpLoader', 'User', '$filter', require('./models/Game') ]);

/* 	==================================
				Services
	================================== */
app.factory('HttpInjector', 			[ 'User', require('./factories/HttpInjector') ]);

app.factory('HttpLoader', 				[ '$q', '$http', require('./factories/HttpLoader') ]);

app.factory('TemplateFactory', 			[ 'HttpLoader', require('./factories/TemplateFactory') ]);

app.factory('GameFactory', 				[ 'HttpLoader', 'Game', 'User', require('./factories/GameFactory') ]);

app.factory('TileFactory', 				[ 'HttpLoader', require('./factories/TileFactory') ]);

app.factory('LayoutFactory', 			[ require('./factories/LayoutFactory') ]);

app.factory('SocketFactory', 			[ '$rootScope', require('./factories/SocketFactory') ]);

/* 	==================================
				Controllers
	================================== */
app.controller('MainController', 		[ 'User', require('./controllers/MainController') ]);

app.controller('GamesController', 		[ '$state', 'GameFactory', 'gamesResponseArray', 'templatesResponse', require('./controllers/GamesController') ]);

app.controller('GameController', 		[ '$scope', 'GameFactory', 'TileFactory', 'SocketFactory', 'game', require('./controllers/GameController') ]);

app.controller('CallbackController', 	[ '$location', require('./controllers/CallbackController') ]);

app.controller('LogoutController', 		[ '$location', require('./controllers/LogoutController') ]);

/* 	==================================
				Directives
	================================== */
app.directive('clickable', 				[ require('./directives/Clickable') ]);

app.directive('tile', 					[ require('./directives/Tile') ]);

app.directive('modal', 					[ '$parse', require('./directives/Modal') ]);

/* 	==================================
				Filters
	================================== */
app.filter("matched", 					[ require('./filters/Matched') ]);

app.filter("state", 					[ require('./filters/State') ]);

app.filter("playedgamesfrom", 			[ require('./filters/PlayedGamesFrom') ]);

app.filter("matchedpairby", 			[ require('./filters/MatchedPairBy') ]);

app.filter("matchedpairs", 				[ require('./filters/MatchedPairs') ]);

/* 	==================================
				Configuration
	================================== */
app.config(['$stateProvider', '$urlRouterProvider', require('./configs/Router') ]);

app.config(['$httpProvider', function ($httpProvider)
{
	$httpProvider.interceptors.push('HttpInjector');
}]);