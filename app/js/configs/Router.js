module.exports 			= function($stateProvider, $urlRouterProvider)
{
	$stateProvider

	.state('main',
	{
		url 			: "/main",
		abstract 		: true,
		templateUrl 	: "partials/menu.html",
	})

	.state('main.games',
	{
		url 			: "/games/:paginationIndex",
		controller 		: "GamesController as gamesCtrl",
		templateUrl 	: "partials/games/games.html",
		resolve: 		{
			gamesResponseArray: function ($stateParams, GameFactory)
			{
				return GameFactory.loadGames($stateParams.paginationIndex);
			},
			templatesResponse: function (TemplateFactory)
			{
				return TemplateFactory.loadTemplates();
			}
		}
	})
	.state('main.games.open',
	{
		url 			: "/open",
		templateUrl 	: "partials/games/games-open.html"
	})
	.state('main.games.playing',
	{
		url 			: "/playing",
		templateUrl 	: "partials/games/games-playing.html"
	})
	.state('main.games.finished',
	{
		url 			: "/finished",
		templateUrl 	: "partials/games/games-finished.html"
	})

	.state('main.games.myopengames',
	{
		url 			: "/myopengames",
		templateUrl 	: "partials/games/games-myopengames.html"
	})
	.state('main.games.myplayinggames',
	{
		url 			: "/myplayinggames",
		templateUrl 	: "partials/games/games-myplayinggames.html"
	})
	.state('main.games.myfinishedgames',
	{
		url 			: "/myfinishedgames",
		templateUrl 	: "partials/games/games-myfinishedgames.html"
	})
	.state('main.games.new',
	{
		url 			: "/new",
		templateUrl 	: "partials/games/games-new.html"
	})

	.state('main.game',
	{
		url 			: "/game/:id",
		controller 		: "GameController as gameCtrl",
		templateUrl 	: "partials/game/game.html",
		resolve			: {
			game: function ($stateParams, GameFactory)
			{
				if ($stateParams)
				{
					gameId 							= $stateParams.id;

					if (!gameId && GameFactory.getWatchingGame())
					{
						return GameFactory.getWatchingGame();
					}
					else if (gameId)
					{
						return GameFactory.loadGame(gameId);
					}
				}
				else
				{
					return null;
				}
			}
		}
	})
	.state('main.game.board',
	{
		url 			: "/board",
		templateUrl 	: "partials/game/game-board.html"
	})
	.state('main.game.players',
	{
		url 			: "/players",
		templateUrl 	: "partials/game/game-players.html"
	})
	.state('main.game.logs',
	{
		url 			: "/logs",
		templateUrl 	: "partials/game/game-logs.html"
	})

	.state('main.callback',
	{
		url 			: "/callback",
		controller 		: "CallbackController"
	})
	.state('main.logout',
	{
		url 			: "/logout",
		controller 		: "LogoutController"
	});

	$urlRouterProvider.otherwise('/main/games/1/open');
};