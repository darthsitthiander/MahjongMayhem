module.exports 		= function ()
{
	return function (arr, state)
	{
		var toReturn = [];

		angular.forEach(arr, function (item)
		{
			if (item.state == state)
			{
				toReturn.push(item);
			}
		});

		return toReturn;
	}
};