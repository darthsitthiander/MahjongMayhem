module.exports 								= function ()
{
	return function (arr, state, username)
	{
		var toReturn 						= [];
		var keepLooping  					= true;

		angular.forEach(arr, function (item)
		{
			if (item.state == state)
			{
				keepLooping  				= true;

				angular.forEach(item.players, function (player)
				{
					if (keepLooping && player._id == username)
					{
						toReturn.push(item);
						keepLooping  		= false;
					}
				});
			}
		});

		return toReturn;
	}
};