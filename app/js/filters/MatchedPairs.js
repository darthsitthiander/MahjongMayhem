module.exports 				= function ()
{
	return function (arr)
	{
		var toReturn 		= [];

		console.log('arr', arr);

		//return arr;

		var foundMatch 		= false;

		angular.forEach(arr, function (level)
		{
			var rows 		= [];

			angular.forEach(level, function (row)
			{
				var tiles 	= [];

				angular.forEach(row, function (tile)
				{
					if (tile.match)
					{
						foundMatch 		= false;

						angular.forEach(toReturn, function (tile2)
						{
							if (tile._id == tile2[0].match.otherTileId)
							{
								tile2.push(tile);
								foundMatch = true;
							}
						},toReturn);

						if (!foundMatch)
						{
							toReturn.push([tile]);
						}
					}

				}, tiles);

			}, rows);

		}, toReturn);

		console.log('filtered', toReturn);

		return toReturn;
	}
};