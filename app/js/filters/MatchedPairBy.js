module.exports 				= function ()
{
	return function (arr, playerId)
	{
		var toReturn 		= [];

		angular.forEach(arr, function (pair)
		{
			if (pair[0].match.foundBy == playerId)
			{
				toReturn.push(pair);
			}
		}, toReturn);

		return toReturn;
	}
};